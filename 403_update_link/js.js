$(document).ready(function () {

    // this only happens on key up
    $('#in').keyup(function(){

        // makes section visible
        $("#out").css("visibility", "visible");

        // form search string
        let outString = "https://google.co.nz/search?q=";
        outString += $('#in').val();

        // replace text with string
        $('#outtext').html(outString);

        // replace href attribute with string
        $('#outlink').attr("href",outString);
    });
  
  });
   