# Nayland Notices
Automated notice system for Nayland College.

This is best viewed in Chrome in full screen mode (F11).

Currently this lives here:

https://notices.digitaldojo.nz/ 


# To clone a copy of this use:
```
git clone git@gitlab.com:edward.pattillo/notices.git .
```
(It's a public repo. The folder it's cloned into needs to be empty. The " ." at the end of the command above is there on purpose.)


# To Do List:


**Ben**: Can we add the staff code? I know mostly this is DHD at the moment, but we would like to open things up for staff to sort their own notices as well.


