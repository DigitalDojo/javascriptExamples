// We're declaring global variables here so that they can be used inside or outside functions.

// The total volume starts out at zero
var totalVolume = 0;

// loadedBoxes is an empty array. We will push boxes onto it 
var loadedBoxes = [];

// Maximum volume of Mick's truck
var maxVolume = 25;

// This is the volume of the small boxes.
var smallVolume = 1;

// mediumVolume is a derived value because it is based on smallVolume.
var mediumVolume = smallVolume + 2;

// largeVolume is a derived value because it is based on smallVolume.
var largeVolume = smallVolume + 4;



// This function's purpose is to add the box name to the list of loaded boxes,
// add the box volume to the total volume then run the output function.
// This function is called when someone clicks one of the small box buttons.
// It is triggered with the onclick="loadBox(this.id)" in the button HTML.
// this.id is the individual id of the button which we are sending as a parameter.
// The function recieves this.id but renames it to boxClicked as it enters the function.
function loadBox(boxClicked) {

  // console.log(boxClicked) allows us to see the incoming id in the console
  console.log(boxClicked);
  
  // this gets the text inside the <button></button> that was clicked
  var boxName = document.getElementById(boxClicked).innerHTML;

  // this pushes the box name onto the end of the global loadedBoxes array
  loadedBoxes.push(boxName);

  // this gets the custom attribute "size" inside the <button></button> that was clicked
  var boxSize = document.getElementById(boxClicked).getAttribute("size");

  // We want to test which size box so that we
  // can add the appropriate volume to the totalVolume. 
  if (boxSize == 'small') {
    
    // This adds the small volume to the total volume.
    // We use += as a shortcut to increment instead of 
    // totalVolume = totalVolume + smallVolume;
    totalVolume += smallVolume;
    
  } else if (boxSize == 'medium') {
    // This adds the medium volume to the total volume.
    totalVolume += mediumVolume;
    
  } else if (boxSize == 'large') {
    // This adds the large volume to the total volume.
    totalVolume += largeVolume;
  }
    
  // look at our loadedBoxes array and totalVolume in the console
  console.log(loadedBoxes);
  console.log("totalVolume: "+totalVolume);

  // test to see if the user has gone over the maximum volume of 
  // Mick's truckif they have then send them a warning by replacing 
  // the HTML  of the h2 element with  id="warning" 
  if (totalVolume > maxVolume) {
    document.getElementById("warning").innerHTML = "You've gone over the total. Reload the page to try again.";
  } 
  
  // run the showOutput function to present the results to the user
  showOutput();
  
} // END OF function loadBox() {








// This function creates and presents the current 
// list of loaded items and the total volume.
function showOutput() {

  // outputHTML stores the HTML that we will eventually output.
  // We are going to add to it as we go and then present it at the end.
  var outputHTML = '<p>The boxes loaded so far are:</p>';
  
  // start an unordered list (bullet points)
  outputHTML += '<ul>';
  
  // This iterates through the loadedBoxes array and adds the name
  // to the list. Each item on loadedBoxes will be a bullet point
  for (i = 0; i < loadedBoxes.length; i++) {
    outputHTML += '<li>'+loadedBoxes[i]+'</li>';
  }
  
  // end the unordered list
  outputHTML += '</ul>';
  
  // We want a running total of the volume so far.
  outputHTML += '<p>Total Volume: '+totalVolume+' cubic metres</p>';
  
  // innerHTML replaces the HTML in the output div with our new HTML.
  document.getElementById("output").innerHTML = outputHTML;
  
}// END OF showOutput() {


