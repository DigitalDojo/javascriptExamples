$(document).ready(function() {

  preload(['images/trumpAfter.jpg']);
  var firstEnterFlag = 1;
  
//   $('body').flowtype({
//     minimum   : 500,
//     maximum   : 1200,
//     minFont   : 12,
//     maxFont   : 40,
//     fontRatio : 30
//   });  
  
  
  $('#voteNo').mouseenter(function() {
    
    $('#voteYes').css('background-color', '#00b300');
    $('#voteNo').css('background-color', '#ff3333');
    
    if (firstEnterFlag == 1)
      {
        firstEnterFlag = 0;
        $("#voteNo").fadeOut(500, function() {
          var $this = $('#voteNo');
          animateButton($this);
          $("#voteNo").fadeIn(500);
        });
      } else 
      {
        var $this = $('#voteNo');
        animateButton($this);
      }

  });

  $('#voteNo').mouseout(function() {

    $('#voteYes').css('background-color', '#000');
    $('#voteNo').css('background-color', '#000');
  });

  $('#voteYes').mouseenter(function() {

    $('#voteYes').css('background-color', '#00b300');
  });

  $('#voteYes').mouseout(function() {

    $('#voteYes').css('background-color', '#000');
  });

  $('#voteYes').click(function() {

    $('#voteYes').fadeOut(1000);
    $('#voteNo').fadeOut(1000);
    $('html').css('transition', 'background 1s linear');
    $('html').css('-webkit-transition', 'background 1s linear');
    $('html').css('-moz-transition', 'background 1s linear');
    $('html').css('-o-transition', 'background 1s linear');
    $('html').css('-ms-transition', 'background 1s linear');
 
    $('html').css('background', 'url(images/trumpAfter.jpg)');
    $('html').css('background-size', 'cover');
    $('html').css('background-position', 'center center');
    $('html').css('background-repeat', 'no-repeat');
    $('html').css('background-attachment', 'fixed');
    $('html').css('background-color', '#000');

    $('#message').fadeOut(600, function() {

      $('#message').html('<p>Thank you for your<br>honest vote of confidence.</p>');
      $('#message').css('font-size','1.1em');
      $('#message').fadeIn(600);
    });
  });

  $('#voteNo').click(function() {
    $("#voteNo").fadeOut(1000, function() {
      var $this = $('#voteNo');
      animateButton($this);
      $("#voteNo").fadeIn(1000);
    });
  });

  function animateButton($this) {
    
    var dWidth = window.innerWidth - ($this.width());
//     var dHeight = window.innerHeight - ($this.height());
    var dHeight = (window.innerHeight * 0.66) - ($this.height());
    var nextX = Math.floor(Math.random() * dWidth);
    var nextY = Math.floor(Math.random() * dHeight) + (window.innerHeight * 0.33);

    $this.animate({
      left: nextX + 'px',
      top: nextY + 'px'
    });
    //     console.log(dHeight);
  }

  function preload(arrayOfImages) {
    $(arrayOfImages).each(function() {
      $('<img />').attr('src', this).appendTo('body').css('display', 'none');
    });
  }
});