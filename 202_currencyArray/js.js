   // here is a function that we've named convertCurrency
   // it just sits here and waits for something to use it.
   // In our case we have an onclick event listener attached
   // to a button on the user interface
   function convertCurrency() {

     // this is an array of hard-coded values of the inverted exchange rates
     // in an actually useful app, these would be updated regularly via an API
     // from left to right the currencies are:   US $, EUR €, GBP £
     var exchangeRates = [1.42389, 1.53125, 1.76595];
     
     // these variables are declared here but assigned inside the conditional
     // according to which radio button the user selected
     var index;
     var abbreviation;

     // if the user selected the US radio button
     if (document.getElementById('currencyType_US').checked) {
       
       // index 0 is the first value on our exchangeRates array that we declared above
       index = 0;
       
       // this is so that we can output the currency and symbol later
       abbreviation = 'US $';

     } else if (document.getElementById('currencyType_EUR').checked) {
       
       // index 1 is the second value on our exchangeRates array that we declared above
       index = 1;
       
       // this is so that we can output the currency and symbol later
       abbreviation = 'EUR €';
       
     } else if (document.getElementById('currencyType_GBP').checked) {
       
       // index 2 is the third value on our exchangeRates array that we declared above
       index = 2;
       
       // this is so that we can output the currency and symbol later
       abbreviation = 'GBP £';
     }

     // this retrieves whatever value the user has typed into our input with the id  "amount"
     var amount = document.getElementById("amount").value;

     // here we make a new variable called "converted" which multiplies the user's 
     // inputted amount by the exchange rate and then rounds it to two decimal places
     var converted = (amount * exchangeRates[index]).toFixed(2);

     // outputText is a concatenated string variable that contains the values
     // that we calculated above. Even though it is partly formed of numeric variables
     // it is treated like a string because it contains textual data
     var outputText = abbreviation + amount + ' = NZ$' + converted;

     // this puts the string we just made above into the empty 
     // div with the id "output" in our HTML doc
     document.getElementById("output").innerHTML = outputText;
   }